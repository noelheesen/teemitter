import EventEmitter from '../src/EventEmitter';

describe('EventEmitter', () => {
  let emitter: EventEmitter<{ test: string }>;

  beforeEach(() => {
    emitter = new EventEmitter<{ test:string }>();
  });

  test('can be instantiated with new', () => {
    expect(emitter).toBeDefined();
  });

  it('can get max listeners', () => {
    expect(emitter.getMaxListeners()).toBeTruthy();
  });

  it('can set max listeners', () => {
    emitter.setMaxListeners(20);
    expect(emitter.getMaxListeners()).toBe(20);
  });

  it('can set max listeners in constructor', () => {
    expect(new EventEmitter<{ test:string }>(10).getMaxListeners()).toBe(10);
  });

  it('has a default of 30 maximum listeners', () => {
    expect(emitter.getMaxListeners()).toBe(30);
  });

  it('listenerCount returns 0 when no listeners', () => {
    expect(emitter.listenerCount()).toBe(0);
  });

  it('can add a listener using addListener', () => {
    emitter.addListener('test', (something: string) => 'works!');
    expect(emitter.listenerCount()).toBe(1);
  });

  it('can add a listner using on', () => {
    emitter.on('test', () => 'works!');
    expect(emitter.listenerCount()).toBe(1);
  });

  it('can get listenerCount for all listeners', () => {
    emitter.on('test', () => 'test!');
    emitter.on('error', () => 'error!');
    expect(emitter.listenerCount()).toBe(2);
  });

  it('can get listenerCount for specific event', () => {
    emitter.on('test', () => 'works!');
    expect(emitter.listenerCount('test')).toBe(1);
  });

  it('can remove a listener using removeListener', () => {
    const listener = (argh: string) => 'pirates!';
    emitter.addListener('test', listener);
    expect(emitter.listenerCount()).toBe(1);
    emitter.removeListener('test', listener);
    expect(emitter.listenerCount()).toBe(0);
  });

  it('can remove a listener using off', () => {
    const listener = (argh: string) => 'pirates!';
    emitter.addListener('test', listener);
    expect(emitter.listenerCount()).toBe(1);
    emitter.off('test', listener);
    expect(emitter.listenerCount()).toBe(0);
  });

  it('can emit event', () => {
    emitter.on('test', () => expect(1).not.toBe(2));
    expect(emitter.listenerCount()).toBe(1);
    emitter.emit('test', 'pirates!');
  });

  it('can emit event to multiple listeners', () => {
    const result = [];
    emitter.on('test', () => result.push(1));
    emitter.on('test', () => result.push(1));
    emitter.emit('test', '2');
    expect(result.length).toBe(2);
  });

  it('listener recieves data from event', () => {
    emitter.on('test', argh => expect(argh).toEqual('pirates!'));
    expect(emitter.listenerCount()).toBe(1);
    emitter.emit('test', 'pirates!');
  });

  it('removes a once listener after invoking it', () => {
    emitter.once('test', argh => argh);
    expect(emitter.listenerCount()).toBe(1);
    emitter.emit('test', 'pirates!');
    expect(emitter.listenerCount()).toBe(0);
  });

  it('prepends a listener', () => {
    let result;
    emitter.addListener('test', () => result = 1);
    emitter.prependListener('test', () => result = 2);
    emitter.emit('test', 'pirates!');
    expect(result).toBe(1);
  });

  it('prepends and removes a once listener', () => {
    let result;
    emitter.addListener('test', () => result = 1);
    emitter.prependOnceListener('test', () => result = 2);
    expect(emitter.listenerCount()).toBe(2);
    emitter.emit('test', 'pirates!');
    expect(emitter.listenerCount()).toBe(1);
  });

  it('returns false when emitting without listeners', () => {
    expect(emitter.emit('error', new Error())).toBe(false);
  });

  it('emits error when max listeners reached', () => {
    emitter.setMaxListeners(1);
    let emittedError = false;

    emitter.setErrorListener(() => emittedError = true);
    emitter.on('error', () => emittedError = true);
    emitter.on('test', () => 2);
    expect(emittedError).toBe(true);
  });

  it('can get a list of event names whith subscribed listeners', () => {
    expect(emitter.eventNames()).toEqual([]);
    emitter.addListener('test', str => void 0);
    expect(emitter.eventNames()).toEqual(['test']);
  });
});
