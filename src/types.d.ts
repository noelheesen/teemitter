
export type EventHandler<T, K extends keyof T> = (arg: T[K]) => any;


