import IDisposable from './IDisposable';
import IEventEmitter from './IEventEmitter';
import { EventHandler } from './types';

/*
 * Copy from nodejs source
 * https://github.com/nodejs/node/blob/master/lib/events.js#L219
 **/
function onceWrapper(this: any, ...args: any[]) {
  if (!this.fired) {
    this.target.removeListener(this.event, this.wrapFn);
    this.fired = true;
    Reflect.apply(this.listener, this.target, args);
  }
}

/*
 * Copy from nodejs source
 * https://github.com/nodejs/node/blob/master/lib/events.js#L227
 **/
function onceWrap<T>(target: IEventEmitter<T>, event: keyof T, listener: (arg: any) => any) {
  const state = {
    target,
    event,
    listener,
    fired: false,
    wrapFn: undefined,
  };

  const wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}



/**
 * Typed EventEmitter, know what get's emitted.
 *
 * @class EventEmitter
 * @implements {IEventEmitter<T>}
 * @template E The type that specifies which events can be emitted
 */
class EventEmitter<E, T = E & { error: Error }> implements IEventEmitter<T>, IDisposable {
  private events: Map<keyof T, Array<EventHandler<T, keyof T>>>;
  private maxListeners: number;
  private _listenerCount = 0;
  private _errorListener?: Function;
  private disposed = false;

  constructor(maxListeners = 30) {
    this.events = new Map<keyof T, Array<EventHandler<T, keyof T>>>();
    this.maxListeners = maxListeners;
  }

  /**
   * Add a listener to one of the events
   *
   * @template K key of T
   * @param {K} event The event the listener should act upon
   * @param {EventHandler<T, K>} listener The listener to be invoked when event is emitted
   * @returns {this}
   * @memberof EventEmitter
   */
  public addListener<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this {
    return this._addListener(event, listener);
  }

  /**
   * Add a listener to one of the events (alias for addListener)
   *
   * @template K key of T
   * @param {K} event The event the listener should act upon
   * @param {EventHandler<T, K>} listener The listener to be invoked when event is emitted
   * @returns {this}
   * @memberof EventEmitter
   */
  public on<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this {
    return this._addListener(event, listener);
  }

  /**
   * Add a listner to one of the events that will be removed after being invoked once.
   * Because of this, it cannot be removed.
   *
   * @template K key of T
   * @param {K} event The event the listener should act upon
   * @param {EventHandler<T, K>} listener The listener to be invoked when event is emitted
   * @returns {this}
   * @memberof EventEmitter
   */
  public once<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this {
    return this._addListener(event, onceWrap(this, event, listener));
  }

  /**
   * Remove a listener from one of the events.
   *
   * @description Once listeners cannot be removed
   *
   * @template K key of T
   * @param {K} event The event from where the listener gets removed
   * @param {EventHandler<T, K>} listener The listener to be removed from the event
   * @returns {this}
   * @memberof EventEmitter
   */
  public removeListener<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this {
    const eventListeners = this.events.get(event);

    if (eventListeners) {
      const listenerIndex = eventListeners.indexOf(listener);
      if (listenerIndex > -1) {
        eventListeners.splice(listenerIndex, 1);
        this._listenerCount = this._listenerCount - 1;
      }
    }

    return this;
  }

  /**
   * Remove a listener from one of the events (alias for removeEventListener)
   *
   * @description Once listeners cannot be removed
   *
   * @template K key of T
   * @param {K} event The event from where the listener gets removed
   * @param {EventHandler<T, K>} listener The listener to be removed from the event
   * @returns {this}
   * @memberof EventEmitter
   */
  public off<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this {
    return this.removeListener(event, listener);
  }

  /**
   * Set the maximum mount of listeners across all events, defaults to 30
   *
   * @param {number} n
   * @returns {this}
   * @memberof EventEmitter
   */
  public setMaxListeners(n: number): this {
    this.maxListeners = n;
    return this;
  }

  /**
   * Get the maxListener amount for this emitter
   *
   * @returns {number}
   * @memberof EventEmitter
   */
  public getMaxListeners(): number {
    return this.maxListeners;
  }

  /**
   * Get all the listeners for an event
   *
   * @template K
   * @param {K} event
   * @returns {Array<EventHandler<T, K>>} An array of existing listeners or a new, empty array if none present
   * @memberof EventEmitter
   */
  public listeners<K extends keyof T>(
    event: K)
    : Array<EventHandler<T, K>> {
    return this.events.get(event) || new Array<EventHandler<T, K>>();
  }

  /**
   * Emit an event
   *
   * @template K Key of T
   * @param {K} event
   * @param {T[K]} arg The argument passed to all listeners
   * @returns {boolean} true if a listener is invoked, false otherwise
   * @memberof EventEmitter
   */
  public emit<K extends keyof T>(
    event: K,
    arg: T[K])
    : boolean {
    const events = this.events.get(event);

    if (events && events.length) {
      // tslint:disable-next-line:no-increment-decrement
      for (let index = 0; index <= events.length; index++) {
        const listener = events[index];
        if (listener) {
          listener(arg);
        }
      }
      return true;
    }

    return false;
  }

  /**
   * Get the listener count
   *
   * @template K
   * @param {K} [event?] optional event
   * @returns {number} the count for a specific event if specified, count of all listeners otherwise
   * @memberof EventEmitter
   */
  public listenerCount<K extends keyof T>(event?: K): number {
    return event ? this.listeners(event).length : this._listenerCount;
  }

  /**
   * Prepend a listener for the event
   *
   * @template K
   * @param {K} event The event the listener should act upon
   * @param {EventHandler<T, K>} listener The listener to be invoked when event is emitted
   * @returns {this}
   * @memberof EventEmitter
   */
  public prependListener<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>): this {
    return this._addListener(event, listener, true);
  }

  /**
   * Prepend a once listener for the event
   *
   * @template K
   * @param {K} event The event the listener should act upon
   * @param {EventHandler<T, K>} listener The listener to be invoked when event is emitted
   * @returns {this}
   * @memberof EventEmitter
   */
  public prependOnceListener<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this {
    return this._addListener(event, onceWrap(this, event, listener), true);
  }

  /**
   * Get all the event names associated with this EventEmitter
   *
   * @returns {(Array<string | symbol>)}
   * @memberof EventEmitter
   */
  public eventNames(): Array<keyof T> {
    return Array.from(this.events.keys());
  }

  /**
   * Dispose the emitter
   *
   * @description release all listener handles
   * @returns {void}
   * @memberof EventEmitter
   */
  public dispose(): void {
    if (this.disposed) {
      throw new Error('EventEmitter is already disposed');
    }

    this.events.forEach((listeners) => {
      while (listeners.length > 0) {
        listeners.pop();
      }
    });
    this.events.clear();
    this._errorListener = undefined;

    this.disposed = true;
  }

  /**
   * Handle errors
   * @param listener
   */
  public setErrorListener(
    listener?: (arg: Error) => any) {
    this._errorListener = listener;
  }

  // tslint:disable function-name
  private _addListener<K extends keyof T>(
    event: K,
    listener: (arg: T[K]) => any,
    prepend = false) {

    if (this._listenerCount >= this.maxListeners) {
      // tslint:disable-next-line
      this.__emitError(new Error(`Possible memory leak detected. The current total listener count is ${this._listenerCount}. Increase the maxListenerCount which is currently ${this.maxListeners} if you want to allow for more listeners`));
    } else {
      let events = this.events.get(event);

      if (!events) {
        events = new Array<EventHandler<T, K>>();
        this.events.set(event, events);
      }
      if (prepend) {
        events.unshift(listener);
      } else {
        events.push(listener);
      }
      this._listenerCount = this._listenerCount + 1;
    }
    return this;
  }

  private __emitError(error: Error) {
    if (this._errorListener) {
      this._errorListener(error);
    } else {
      console.warn(error);
    }
  }
}

export default EventEmitter;
