import { EventHandler } from './types';

/**
 * IEventEmitter interface for a typed event emitter
 *
 * @export
 * @interface IEventEmitter
 * @template T
 */
export default interface IEventEmitter<T> {

  /**
   * Adds a listener for the specified event
   *
   * @template K keyof T
   * @param {K} event
   * @param {EventHandler<T, K>} listener
   * @returns {this}
   * @memberof IEventEmitter
   */
  addListener<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this;

  /**
   * Alias for addListener
   *
   * @template K
   * @param {K|E} event
   * @param {(arg: T[K]|IEmitErrors[E]) => any} listener
   * @returns {this}
   * @memberof IEventEmitter
   */
  on<K extends keyof T>(event: K, listener: EventHandler<T, K>): this;

  /**
   * Add a listener for the specified event that removes itself after triggered once
   *
   * @template K
   * @param {K|E} event
   * @param {(arg: T[K]|IEmitErrors[E]) => any} listener
   * @returns {this}
   * @memberof IEventEmitter
   */
  once<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this;

  /**
   * Remove a listener for the specified event, does not work with once listeners
   *
   * @template K
   * @param {K|E} event
   * @param {(arg: T[K]|IEmitErrors[E]) => any} listener
   * @returns {this}
   * @memberof IEventEmitter
   */
  removeListener<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this;

  /**
   * Alias for removeListener
   *
   * @template K
   * @param {K|E} event
   * @param {(arg: T[K]|IEmitErrors[E]) => any} listener
   * @returns {this}
   * @memberof IEventEmitter
   */
  off<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this;

  /**
   * Set the maximum amount of listeners, used to detect possible memory leaks
   *
   * @param {number} n
   * @returns {this}
   * @memberof IEventEmitter
   */
  setMaxListeners(n: number): this;

  /**
   * Get the maximum amount of listeners allowed
   *
   * @returns {number}
   * @memberof IEventEmitter
   */
  getMaxListeners(): number;

  /**
   * Get all listeners associated with an event
   *
   * @template K
   * @param {K|E} event
   * @returns {Array<(arg: T[K]|IEmitErrors[E]) => any>}
   * @memberof IEventEmitter
   */
  listeners<K extends keyof T>(event: K): Array<EventHandler<T, K>>;

  /**
   * Emit an event
   *
   * @template K
   * @param {K|E} event
   * @param {T[K]|IEmitErrors[E]} arg
   * @returns {boolean}
   * @memberof IEventEmitter
   */
  emit<K extends keyof T>(event: K, arg: T[K]): boolean;

  /**
   * Get the listener count
   *
   * @template K
   * @param {K|E} [event]
   * @returns {number}
   * @memberof IEventEmitter
   */
  listenerCount<K extends keyof T>(event?: K): number;

  /**
   * Add a listner to an event but prepend it to the existing listeners
   *
   * @template K
   * @param {K|E} event
   * @param {(arg: T[K]|IEmitErrors[E]) => any} listener
   * @returns {this}
   * @memberof IEventEmitter
   */
  prependListener<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this;

  /**
   * Add a listener for the specified event that removes itself after triggered once
   * but prepend it to the existing listeners
   *
   * @template K
   * @param {K|E} event
   * @param {(arg: T[K]|IEmitErrors[E]) => any} listener
   * @returns {this}
   * @memberof IEventEmitter
   */
  prependOnceListener<K extends keyof T>(
    event: K,
    listener: EventHandler<T, K>)
    : this;

  /**
   * Get an array of all event names
   */
  eventNames(): Array<keyof T>;
}
